import axios from 'axios'

// login页登录
export const login = params => {
  return axios.post('/user/login/', params)
}

export const create = params => {
  return axios.post('/user/create', params)
}

// home页的导航栏
export const menus = () => { return axios.get('/menus') }

// userlist 用户列表页
export const userList = params => {
  return axios.get('/entry/page')
}
// userlist 用户列表页
export const userListv2 = id => {
  return axios.get(`/entry/child/${id}`)
}

// 用户状态修改 put users/:id/state/:type
export const userStateChange = (userId, userStatus) => {
  return axios.put(`/users/${userId}/state/${userStatus}`)
}

// 用户添加 post users?username=admin123&password=123456&email=112@qq.com&mobile=13245678911
export const userAdd = params => {
  return axios.post('/entry/create', params)
}

// 根据id获取单个用户
export const getUserId = userId => { return axios.get(`/entry/${userId}`) }

// 根据用户删除
export const deleteUserId = userId => { return axios.get(`/entry/delete/${userId}`) }

// 编辑用户提交
export const userUpdate = (userId, params) => {
  return axios.post(`/entry/updateById`, params)
}

// 获取权限列表
export const getRightsList = () => { return axios.get(`rights/list`) }
export const getRightsTree = () => { return axios.get(`rights/tree`) }

// 获取角色列表
export const getRolesList = () => { return axios.get(`roles`) }

// 删除权限信息
export const deleteRights = (roleId, rightId) => { return axios.delete(`roles/${roleId}/rights/${rightId}`) }

// 角色授权
export const authorizRights = (roleId, idStr) => { return axios.post(`roles/${roleId}/rights`, { 'rids': idStr }) }

// 创建角色
export const createRoles = params => { return axios.post(`roles/`, params) }

// 根据 ID 查询角色
export const getRole = (roleId) => { return axios.get(`roles/${roleId}`) }

// 编辑角色
export const editRole = (roleId, params) => { return axios.put(`roles/${roleId}`, params) }

// 删除角色
export const destroyRole = roleId => { return axios.delete(`roles/${roleId}`) }

// 商品类别
export const getCategories = (params) => { return axios.get('categories/', { params }) }

// 根据id查询类别
export const getCategorie = (categId) => { return axios.get(`categories/${categId}`) }

// 创建分类
export const createCategorie = (params) => { return axios.post(`categories`, params) }

// 编辑分类
export const editCategorie = (roleId, params) => { return axios.put(`categories/${roleId}`, params) }

// 删除分类
export const destroyCategorie = (roleId) => { return axios.delete(`categories/${roleId}`) }

// 分类参数列表
export const getattributesList = (cateId, sel) => { return axios.get(`categories/${cateId}/attributes`, { params: { sel } }) }

// 添加动态参数或者静态属性
// export const createAttributes = (cateId, params) => { return axios.get(`categories/${cateId}/attributes`, params) }
