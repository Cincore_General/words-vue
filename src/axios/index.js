import axios from 'axios'
let loading
// 设置axios的基础url
axios.defaults.baseURL = 'http://127.0.0.1:8080'
// 所有请求带上Token
axios.interceptors.request.use(config => {
  loading = window.vi.$loading.service({
    lock: true,
    text: 'Loading',
    spinner: 'el-icon-loading',
    background: 'rgba(0, 0, 0, 0.7)'
  })
  // 请求对象添加Authorization
  config.headers.Authorization = window.sessionStorage.getItem('token')
  return config
})

// 添加响应拦截器
axios.interceptors.response.use(function (response) {
  // 对响应数据做点什么
  loading.close()
  return response
}, function (error) {
  // 对响应错误做点什么
  return Promise.reject(error)
})
