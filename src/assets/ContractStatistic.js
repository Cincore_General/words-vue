import { commonService } from '@/service/commonService.js'

export default {
  name: 'ContractStatistic',
  data () {
    return {
      statisticDatas: [],
      pastDays: 0
    }
  },

  mounted () {
    let self = this
    self.loadStatisticData()
  },

  methods: {
    _processLevelStatisticData (dataArray) {
      let self = this

      let resultArray = [] // 一级栏目
      let level = 1
      for (let i = 0; i < dataArray.length; i++) {
        let item = dataArray[i]
        // 查找一级分类
        if (item.parentcategoryid === 0) {
          item['level'] = level
          resultArray.push(item)
          self._loadChildrenData(resultArray, dataArray, item, level + 1)
        }
      }
      return resultArray
    },

    _loadChildrenData (resultArray, originArray, item, level) {
      let self = this

      let deployed = 0
      let undeployed = 0
      let edit = 0
      let completed = 0

      for (let i = 0; i < originArray.length; i++) {
        let originItem = originArray[i]
        // 判断是否是item项的子项
        if (originItem.parentcategoryid === item.categoryid) {
          item['hasChildren'] = true
          item['showChildren'] = false

          originItem['level'] = level
          originItem['visible'] = false // 子项默认隐藏
          originItem['hiddenByCategory'] = false

          resultArray.push(originItem)
          self._loadChildrenData(
            resultArray,
            originArray,
            originItem,
            level + 1
          )

          // 应该放在统计完子类数据之后
          deployed += originItem.deployed
          undeployed += originItem.undeployed
          edit += originItem.edit
          completed += originItem.completed
        }
      }
      // 计算子栏目统计合计
      item.deployed += deployed
      item.undeployed += undeployed
      item.edit += edit
      item.completed += completed
    },

    search () {
      console.log(this.pastDays)
      this.loadStatisticData()
    },

    loadStatisticData () {
      let self = this
      let params = {
        pastDays: self.pastDays
      }
      commonService.getGeneralStatistics(params).then(function (response) {
        let result = response.data
        let resultArray = self._processLevelStatisticData(result)
        self.statisticDatas = resultArray
      })
    },

    rowClassNameHandler ({ row, rowIndex }) {
      // console.log(row['visible'])
      let className = 'pid-' + row.parentcategoryid
      if (
        row.parentcategoryid !== 0 &&
        (row['visible'] !== true || row['hiddenByCategory'] === true)
      ) {
        className += ' hiddenRow'
      }
      return className
    },

    onExpand (row) {
      let self = this

      let isShowChildren = !row['showChildren']
      row['showChildren'] = isShowChildren
      self._loadAllSubItems(row, true, isShowChildren)
    },

    _loadAllSubItems (item, isFirstLevlChildren, isShowChilren) {
      let self = this
      let dataArray = []
      for (let i = 0; i < self.statisticDatas.length; i++) {
        let tempItem = self.statisticDatas[i]
        if (tempItem.parentcategoryid === item.categoryid) {
          if (isFirstLevlChildren) {
            tempItem['visible'] = !tempItem['visible']
          }
          tempItem['hiddenByCategory'] = !isShowChilren

          dataArray.push(tempItem)
          let subItemArray = self._loadAllSubItems(
            tempItem,
            false,
            isShowChilren
          )
          dataArray = dataArray.concat(subItemArray)
        }
      }
      return dataArray
    }
  }
}
