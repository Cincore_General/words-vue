import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../components/Login.vue'
import Home from '../components/Home.vue'
import Welcome from '../components/Welcome.vue'
import UserList from '../components/UserList.vue'
import UserSubList from '../components/UserSubList'

Vue.use(VueRouter)

const routes = [{
  path: '/home',
  name: 'home',
  component: Home,
  redirect: '/welcome',
  children: [{
    path: '/welcome',
    component: Welcome
  },
    {
      path: '/users',
      component: UserList
    },
    {
      path: '/usersid',
      component: UserSubList
    }
  ]
},
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    // 默认 跳转
    path: '/',
    redirect: '/login'
  }
]

const router = new VueRouter({
  routes
})

// 路由守卫 跳转之前的逻辑
router.beforeEach((to, from, next) => {
  const token = window.sessionStorage.getItem('token')
  // 判断是否有token
  if (to.name !== 'login' && !token) {
    return next('/login')
    // 判断有了token是否是请求的login页
  } else if (to.path === '/login' && token) return next(false)
  next()
})

export default router
