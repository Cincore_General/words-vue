// 入口文件
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'

// 导入css
import './assets/css/common.css'
import 'element-ui/lib/theme-chalk/index.css'
// 导入字体样式css
import './assets/fonts/fonts/iconfont.css'

// 导入axios
import './axios/'
import Axios from 'axios'

// 把axios 绑定到vue原型上
Vue.prototype.$http = Axios

Vue.config.productionTip = false

window.vi = new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
