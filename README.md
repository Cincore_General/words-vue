# vue-shop

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### 建立新的分支
1. 开发一个功能建立一个新的分支
2. 便以后期维护
- 建立新的分支
```shell
git checkout -b login
```
- 检查现在有多少分支
```shell
git branch
```
3. 分支改名
```
git branch -m branch_name new_barnch_name
```

### 